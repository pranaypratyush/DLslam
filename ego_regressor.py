import torch
import torch.nn as nn
import sparseconvnet as scn


# from data import get_iterators

# two-dimensional SparseConvNet
class Model(nn.Module):
	def __init__(self):
		nn.Module.__init__(self)
		self.sparseModel = scn.Sequential(
			scn.DenseToSparse(3),
			scn.SubmanifoldConvolution(3, 3, 96, 3, False),
			scn.BatchNormReLU(96),
			scn.UNet(3, 2, [64, 128, 192, 256], residual_blocks=True, downsample=[2, 2, 2]),
			scn.Convolution(3, 1, 96),
			scn.BatchNormReLU(96),
			scn.SparseToDense(3)
		)
		self.linear1 = nn.Sequential(
			nn.Linear(192, 128),
			nn.ReLU(),
			nn.Linear(128, 128),
			nn.Relu(),
			nn.Linear(128, 6),
		)
		self.linear2 = nn.Sequential(
			nn.Linear(192, 128),
			nn.ReLU(),
			nn.Linear(128, 96),
			nn.Relu(),
			nn.Linear(96, 3),
		)

	def forward(self, x):
		x = self.inputLayer(x)
		x = self.sparseModel(x)
		x = x.view(-1, 64)
		x = self.linear(x)
		return x


model = Model()
