import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
from torch.distributions.normal import Normal
# from torch.autograd import Variable
import torch.nn.functional as F
import skimage
import skimage.io
import skimage.transform
import numpy as np
import time
import math
# from input.depth_input import KITTIloader2015 as ls
# from input.depth_input import KITTILoader as DA
from PIL import Image
from depth import *
from input.depth_input import SceneFlowLoader
from input.depth_input import KITTILoader
from depth_train import var_model
# from  import convbn_3d
from input import preprocess

ttype = torch.float

parser = argparse.ArgumentParser(description='PSMNet')
parser.add_argument('--maxdisp', type=int, default=192,
                    help='maxium disparity')
parser.add_argument('--model', default='stackhourglass',
                    help='select model')
parser.add_argument('--datatype', default='2015',
                    help='datapath')
parser.add_argument('--datapath', default='/media/jiaren/ImageNet/data_scene_flow_2015/training/',
                    help='datapath')
parser.add_argument('--epochs', type=int, default=300,
                    help='number of epochs to train')
parser.add_argument('--loadmodel', default='./trained/submission_model.tar',
                    help='load model')
parser.add_argument('--savemodel', default='./',
                    help='save model')
parser.add_argument('--varmodel', default='./',
                    help='save model')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
torch.manual_seed(args.seed)
if args.cuda:
	torch.cuda.manual_seed(args.seed)

if args.datatype == '2015':
	from input.depth_input import KITTI2015_listloader as ls
elif args.datatype == '2012':
	from input.depth_input import KITTI2012_listloader as ls
elif args.datatype == 'sceneflow':
	from input.depth_input import scene_listloader as ls

all_left_img, all_right_img, all_left_disp, test_left_img, test_right_img, test_left_disp = ls(args.datapath)

TrainImgLoader = torch.utils.data.DataLoader(
	KITTILoader(all_left_img, all_right_img, all_left_disp, True),
	batch_size=2, shuffle=True, num_workers=2, drop_last=False)

TestImgLoader = torch.utils.data.DataLoader(
	KITTILoader(test_left_img, test_right_img, test_left_disp, False),
	batch_size=1, shuffle=False, num_workers=1, drop_last=False)

model = PSMNet(args.maxdisp)

if args.cuda:
	model = nn.DataParallel(model)

if args.loadmodel is not None:
	state_dict = torch.load(args.loadmodel)
	model.load_state_dict(state_dict['state_dict'])

if args.varmodel is not None:
	state_dict = torch.load(args.varmodel)
	var_model.load_state_dict(state_dict['state_dict'])

maxdisp = args.maxdisp
DR = disparityregression(maxdisp)


def test(imgL, imgR):
	model.eval()

	if args.cuda:
		imgL = torch.FloatTensor(imgL).cuda()
		imgR = torch.FloatTensor(imgR).cuda()

	imgL, imgR = imgL, imgR

	with torch.no_grad():
		output = model(imgL, imgR)
		print(output.size())
		var = var_model(output)
		output = DR(output)
	output, var = torch.squeeze(output), torch.squeeze(var)
	pred_disp = output.data.cpu().numpy()
	var = var.data.cpu().numpy()
	return pred_disp, var


def main():
	processed = preprocess.get_transform(augment=False)

	for inx in range(len(test_left_img)):
		print(test_left_img[inx])
		# imgL_o = (skimage.io.imread(test_left_img[inx]).astype('float32'))
		# imgR_o = (skimage.io.imread(test_right_img[inx]).astype('float32'))
		imgL_o = np.array(Image.open(test_left_img[inx]))  # .astype('float32')
		imgR_o = np.array(Image.open(test_right_img[inx]))  # .astype('float32')
		# print(imgL_o.shape)
		imgL_ = processed(imgL_o).numpy()
		imgR_ = processed(imgR_o).numpy()
		# print(imgL_.shape)
		# imgL_, imgR_  = imgL_o, imgR_o
		imgL = np.reshape(imgL_, [1, 3, imgL_.shape[1], imgL_.shape[2]])
		imgR = np.reshape(imgR_, [1, 3, imgR_.shape[1], imgR_.shape[2]])
		# pad to (384, 1248)
		top_pad = 384 - imgL.shape[2]
		left_pad = 1248 - imgL.shape[3]
		imgL = np.lib.pad(imgL, ((0, 0), (0, 0), (top_pad, 0), (0, left_pad)), mode='constant', constant_values=0)
		imgR = np.lib.pad(imgR, ((0, 0), (0, 0), (top_pad, 0), (0, left_pad)), mode='constant', constant_values=0)
		# print(imgL.shape)

		start_time = time.time()
		pred_disp, var = test(imgL, imgR)
		#    print(pred_disp)
		print('time = %.2f' % (time.time() - start_time))

		top_pad = 384 - imgL_o.shape[0]
		left_pad = 1248 - imgL_o.shape[1]
		# img = pred_disp[0, top_pad:, :-left_pad]
		img, var_img = pred_disp[top_pad:, :-left_pad], var[top_pad:, :-left_pad]
		#    print(img)
		# var = pred_disp[1, top_pad:, :-left_pad]
		#    s= np.std(var)
		# var[-1, 0] = 0
		var -= np.min(var)
		max = np.max(var)
		var = np.round(var / max * 255).astype('uint8 ')
		# var = ((var - m) / s * 200 + np.min(var))
		# m = np.mean(var)
		# s = np.std(var)
		# print(var.astype('uint16'))
		#    print(np.mean(img))
		#    print(np.std(img))
		# print(m)
		# print(s)
		print(var)
		name = "out/" + test_left_img[inx].split('/')[-1]
		skimage.io.imsave(name, (img * 1).astype('uint8'))
		name_var = name.split('.')[-2] + "_var." + name.split('.')[-1]
		#    print(name_var)
		#    skimage.io.imsave(name_var,((np.exp(var))*256).astype('uint16'))
		skimage.io.imsave(name_var, (var).astype('uint8'))
		# print(var.astype('uint8'))
		name_L = name.split('.')[-2] + "_L." + name.split('.')[-1]
		name_Lo = name.split('.')[-2] + "_Lo." + name.split('.')[-1]


# skimage.io.imsave(name_L, (imgL_).astype('uint16'))
# skimage.io.imsave(name_Lo, (imgL_o).astype('uint8'))


# print(imgL_o)


if __name__ == '__main__':
	main()
