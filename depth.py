import torch
import torch.nn as nn
import torch.utils.data
# from torch.autograd import Variable
import torch.nn.functional as F
import math
import numpy as np

ttype = torch.float


def convbn(in_planes, out_planes, kernel_size, stride, pad, dilation):
	return nn.Sequential(nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride,
	                               padding=dilation if dilation > 1 else pad, dilation=dilation, bias=False),
	                     nn.BatchNorm2d(out_planes))


def convbn_3d(in_planes, out_planes, kernel_size, stride, pad):
	return nn.Sequential(
		nn.Conv3d(in_planes, out_planes, kernel_size=kernel_size, padding=pad, stride=stride, bias=False),
		nn.BatchNorm3d(out_planes))


class hourglass(nn.Module):
	def __init__(self, inplanes):
		super(hourglass, self).__init__()

		self.conv1 = nn.Sequential(convbn_3d(inplanes, inplanes * 2, kernel_size=3, stride=2, pad=1),
		                           nn.ReLU(inplace=True))

		self.conv2 = convbn_3d(inplanes * 2, inplanes * 2, kernel_size=3, stride=1, pad=1)

		self.conv3 = nn.Sequential(convbn_3d(inplanes * 2, inplanes * 2, kernel_size=3, stride=2, pad=1),
		                           nn.ReLU(inplace=True))

		self.conv4 = nn.Sequential(convbn_3d(inplanes * 2, inplanes * 2, kernel_size=3, stride=1, pad=1),
		                           nn.ReLU(inplace=True))

		self.conv5 = nn.Sequential(
			nn.ConvTranspose3d(inplanes * 2, inplanes * 2, kernel_size=3, padding=1, output_padding=1, stride=2,
			                   bias=False),
			nn.BatchNorm3d(inplanes * 2))  # +conv2

		self.conv6 = nn.Sequential(
			nn.ConvTranspose3d(inplanes * 2, inplanes, kernel_size=3, padding=1, output_padding=1, stride=2,
			                   bias=False),
			nn.BatchNorm3d(inplanes))  # +x

	def forward(self, x, presqu, postsqu):

		out = self.conv1(x)  # in:1/4 out:1/8
		pre = self.conv2(out)  # in:1/8 out:1/8
		if postsqu is not None:
			pre = F.relu(pre + postsqu, inplace=True)
		else:
			pre = F.relu(pre, inplace=True)

		out = self.conv3(pre)  # in:1/8 out:1/16
		out = self.conv4(out)  # in:1/16 out:1/16

		if presqu is not None:
			post = F.relu(self.conv5(out) + presqu, inplace=True)  # in:1/16 out:1/8
		else:
			post = F.relu(self.conv5(out) + pre, inplace=True)

		out = self.conv6(post)  # in:1/8 out:1/4

		return out, pre, post


class BasicBlock(nn.Module):
	expansion = 1

	def __init__(self, inplanes, planes, stride, downsample, pad, dilation):
		super(BasicBlock, self).__init__()

		self.conv1 = nn.Sequential(convbn(inplanes, planes, 3, stride, pad, dilation),
		                           nn.ReLU(inplace=True))

		self.conv2 = convbn(planes, planes, 3, 1, pad, dilation)

		self.downsample = downsample
		self.stride = stride

	def forward(self, x):
		out = self.conv1(x)
		out = self.conv2(out)

		if self.downsample is not None:
			x = self.downsample(x)

		out += x

		return out


class feature_extraction(nn.Module):
	def __init__(self):
		super(feature_extraction, self).__init__()
		self.inplanes = 32
		self.firstconv = nn.Sequential(convbn(3, 32, 3, 2, 1, 1),
		                               nn.ReLU(inplace=True),
		                               convbn(32, 32, 3, 1, 1, 1),
		                               nn.ReLU(inplace=True),
		                               convbn(32, 32, 3, 1, 1, 1),
		                               nn.ReLU(inplace=True))

		self.layer1 = self._make_layer(BasicBlock, 32, 3, 1, 1, 1)
		self.layer2 = self._make_layer(BasicBlock, 64, 16, 2, 1, 1)
		self.layer3 = self._make_layer(BasicBlock, 128, 3, 1, 1, 1)
		self.layer4 = self._make_layer(BasicBlock, 128, 3, 1, 1, 2)

		self.branch1 = nn.Sequential(nn.AvgPool2d((64, 64), stride=(64, 64)),
		                             convbn(128, 32, 1, 1, 0, 1),
		                             nn.ReLU(inplace=True))

		self.branch2 = nn.Sequential(nn.AvgPool2d((32, 32), stride=(32, 32)),
		                             convbn(128, 32, 1, 1, 0, 1),
		                             nn.ReLU(inplace=True))

		self.branch3 = nn.Sequential(nn.AvgPool2d((16, 16), stride=(16, 16)),
		                             convbn(128, 32, 1, 1, 0, 1),
		                             nn.ReLU(inplace=True))

		self.branch4 = nn.Sequential(nn.AvgPool2d((8, 8), stride=(8, 8)),
		                             convbn(128, 32, 1, 1, 0, 1),
		                             nn.ReLU(inplace=True))

		self.lastconv = nn.Sequential(convbn(320, 128, 3, 1, 1, 1),
		                              nn.ReLU(inplace=True),
		                              nn.Conv2d(128, 32, kernel_size=1, padding=0, stride=1, bias=False))

	def _make_layer(self, block, planes, blocks, stride, pad, dilation):
		downsample = None
		if stride != 1 or self.inplanes != planes * block.expansion:
			downsample = nn.Sequential(
				nn.Conv2d(self.inplanes, planes * block.expansion,
				          kernel_size=1, stride=stride, bias=False),
				nn.BatchNorm2d(planes * block.expansion), )

		layers = []
		layers.append(block(self.inplanes, planes, stride, downsample, pad, dilation))
		self.inplanes = planes * block.expansion
		for i in range(1, blocks):
			layers.append(block(self.inplanes, planes, 1, None, pad, dilation))

		return nn.Sequential(*layers)

	def forward(self, x):
		x = x.type(ttype)
		output = self.firstconv(x)
		output = self.layer1(output)
		output_raw = self.layer2(output)
		output = self.layer3(output_raw)
		output_skip = self.layer4(output)

		output_branch1 = self.branch1(output_skip)
		output_branch1 = F.interpolate(output_branch1, (output_skip.size()[2], output_skip.size()[3]),
		                               mode='bilinear',
		                               align_corners=True)

		output_branch2 = self.branch2(output_skip)
		output_branch2 = F.interpolate(output_branch2, (output_skip.size()[2], output_skip.size()[3]),
		                               mode='bilinear',
		                               align_corners=True)

		output_branch3 = self.branch3(output_skip)
		output_branch3 = F.interpolate(output_branch3, (output_skip.size()[2], output_skip.size()[3]),
		                               mode='bilinear',
		                               align_corners=True)

		output_branch4 = self.branch4(output_skip)
		output_branch4 = F.interpolate(output_branch4, (output_skip.size()[2], output_skip.size()[3]),
		                               mode='bilinear',
		                               align_corners=True)

		output_feature = torch.cat(
			(output_raw, output_skip, output_branch4, output_branch3, output_branch2, output_branch1), 1)
		output_feature = self.lastconv(output_feature)

		return output_feature


class disparityregression(nn.Module):
	def __init__(self, maxdisp):
		super(disparityregression, self).__init__()
		# maxdisp = int(maxdisp/4)
		# self.disp = torch.Tensor(np.reshape(np.array(range(maxdisp)),[1,maxdisp,1,1])).requires_grad_(False).cuda()
		ser = torch.Tensor(range(maxdisp))
		ser.requires_grad_(False)
		# ser.type(ttype)
		# # ser.
		# # ser.cuda()
		self.disp = torch.reshape(ser, (1, maxdisp, 1, 1))
		self.disp = self.disp.type(ttype).cuda()
		self.disp.requires_grad_(False)
		# print(self.disp)

	# self.pdf_reg = nn.Sequential(nn.Conv2d(maxdisp, int(maxdisp / 4), 1),
	#                              # nn.BatchNorm2d(int(maxdisp / 4)),
	#                              nn.ELU(),
	#                              nn.Conv2d(int(maxdisp / 4), 2, 1))

	def forward(self, x):
		# x,var = x[:,0,:,:], x[:,1,:,:]
		# print(x.size())
		# disp = self.disp.repeat(x.size()[0],1,x.size()[2],x.size()[3],x.size()[4])
		# disp = self.disp.repeat(x.size()[0],x.size()[1],x.size()[2],x.size()[3])
		# mean = torch.sum(x*disp,1)
		# mean = x * disp
		# tt = disp-mean
		# var = torch.sum(tt*tt * x,1)
		# return torch.sum(mean,1), var
		# return torch.sum(x*disp,1)
		# maxdisp = x.size()[2]
		# self.disp = Variable(torch.Tensor(np.reshape(np.array(range(maxdisp)), [1, 1, maxdisp, 1, 1])).cuda(),
		#                      requires_grad=False)
		# print(x.size())
		# print(self.disp.size())
		# x.type(ttype)
		# scaled_x = x * self.disp
		# print(x.type())
		# print(self.disp.type())
		scaled_x = x * self.disp
		# print(scaled_x.size())
		pred = torch.sum(scaled_x, 1)
		# print(pred.size())
		return pred

# return self.pdf_reg(x)


class PSMNet(nn.Module):
	def __init__(self, maxdisp):
		super(PSMNet, self).__init__()
		self.maxdisp = maxdisp
		self.feature_extraction = feature_extraction()
		self.dres0 = nn.Sequential(convbn_3d(64, 32, 3, 1, 1),
		                           nn.ReLU(inplace=True),
		                           convbn_3d(32, 32, 3, 1, 1),
		                           nn.ReLU(inplace=True))

		self.dres1 = nn.Sequential(convbn_3d(32, 32, 3, 1, 1),
		                           nn.ReLU(inplace=True),
		                           convbn_3d(32, 32, 3, 1, 1))
		self.dres2 = hourglass(32)
		self.dres3 = hourglass(32)
		self.dres4 = hourglass(32)
		self.classif1 = nn.Sequential(convbn_3d(32, 32, 3, 1, 1),
		                              nn.ReLU(inplace=True),
		                              nn.Conv3d(32, 1, kernel_size=3, padding=1, stride=1, bias=False))
		self.classif2 = nn.Sequential(convbn_3d(32, 32, 3, 1, 1),
		                              nn.ReLU(inplace=True),
		                              nn.Conv3d(32, 1, kernel_size=3, padding=1, stride=1, bias=False))
		self.classif3 = nn.Sequential(convbn_3d(32, 32, 3, 1, 1),
		                              nn.ReLU(inplace=True),
		                              nn.Conv3d(32, 1, kernel_size=3, padding=1, stride=1, bias=False))
		# self.disparityregression = disparityregression(maxdisp)

		for m in self.modules():
			if isinstance(m, nn.Conv2d):
				n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
				m.weight.data.normal_(0, math.sqrt(2. / n))
			elif isinstance(m, nn.Conv3d):
				n = m.kernel_size[0] * m.kernel_size[1] * m.kernel_size[2] * m.out_channels
				m.weight.data.normal_(0, math.sqrt(2. / n))
			elif isinstance(m, nn.BatchNorm2d):
				m.weight.data.fill_(1)
				m.bias.data.zero_()
			elif isinstance(m, nn.BatchNorm3d):
				m.weight.data.fill_(1)
				m.bias.data.zero_()
			elif isinstance(m, nn.Linear):
				m.bias.data.zero_()

	def forward(self, left, right):

		refimg_fea = self.feature_extraction(left)
		targetimg_fea = self.feature_extraction(right)

		# matching
		# print(self.maxdisp/4)
		cost = torch.zeros((refimg_fea.size()[0], refimg_fea.size()[1] * 2, int(self.maxdisp / 4),
		                    refimg_fea.size()[2], refimg_fea.size()[3]), dtype=ttype,
		                   device=torch.device('cuda'))

		for i in range(int(self.maxdisp / 4)):
			if i > 0:
				cost[:, :refimg_fea.size()[1], i, :, i:] = refimg_fea[:, :, :, i:]
				cost[:, refimg_fea.size()[1]:, i, :, i:] = targetimg_fea[:, :, :, :-i]
			else:
				cost[:, :refimg_fea.size()[1], i, :, :] = refimg_fea
				cost[:, refimg_fea.size()[1]:, i, :, :] = targetimg_fea
		cost = cost.contiguous()

		cost0 = self.dres0(cost)
		cost0 = self.dres1(cost0) + cost0

		out1, pre1, post1 = self.dres2(cost0, None, None)
		out1 = out1 + cost0

		out2, pre2, post2 = self.dres3(out1, pre1, post1)
		out2 = out2 + cost0

		out3, pre3, post3 = self.dres4(out2, pre1, post2)
		out3 = out3 + cost0

		cost1 = self.classif1(out1)
		cost2 = self.classif2(out2) + cost1
		cost3 = self.classif3(out3) + cost2

		if self.training:
			cost1 = F.interpolate(cost1, [self.maxdisp, left.size()[2], left.size()[3]], mode='trilinear',
			                      align_corners=True)
			cost2 = F.interpolate(cost2, [self.maxdisp, left.size()[2], left.size()[3]], mode='trilinear',
			                      align_corners=True)

			# print("cost size")
			# print(cost1.size())
			cost1 = torch.squeeze(cost1, 1)
			pred1 = F.softmax(cost1, dim=1)
			# pred1 = self.disparityregression(pred1)

			cost2 = torch.squeeze(cost2, 1)
			pred2 = F.softmax(cost2, dim=1)
			# pred2 = self.disparityregression(pred2)

		cost3 = F.interpolate(cost3, [self.maxdisp, left.size()[2], left.size()[3]], mode='trilinear',
		                      align_corners=True)
		cost3 = torch.squeeze(cost3, 1)
		cost3 = F.softmax(cost3, dim=1)
		# pred3 = self.disparityregression(cost3)
		# var = ( var2 + var3)/3
		# pred1[:,1], pred2[:,1], pred3[:,1] = F.sigmoid(pred1[:,1]), F.sigmoid(pred2[:,1]), F.sigmoid(pred3[:,1])
		# pred3[:, 1] = cost3[:, 0, pred3[:, 0]]
		# if self.training:
		# 	return pred1, pred2, pred3
		# else:
		# 	return cost3, pred3
		if self.training:
			return pred1, pred2, cost3
		else:
			return cost3