import torch.utils.data as data
from PIL import Image
import os
import os.path
import numpy as np
from . import readpfm
import random
from . import preprocess

IMG_EXTENSIONS = [
	'.jpg', '.JPG', '.jpeg', '.JPEG',
	'.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def is_image_file(filename):
	return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


def KITTI2015_listloader(filepath):
	left_fold = 'image_2/'
	right_fold = 'image_3/'
	disp_L = 'disp_occ_0/'
	disp_R = 'disp_occ_1/'

	image = [img for img in os.listdir(filepath + left_fold) if img.find('_10') > -1]

	train = image[:160]
	val = image[160:]

	left_train = [filepath + left_fold + img for img in train]
	right_train = [filepath + right_fold + img for img in train]
	disp_train_L = [filepath + disp_L + img for img in train]
	# disp_train_R = [filepath+disp_R+img for img in train]

	left_val = [filepath + left_fold + img for img in val]
	right_val = [filepath + right_fold + img for img in val]
	disp_val_L = [filepath + disp_L + img for img in val]
	# disp_val_R = [filepath+disp_R+img for img in val]

	return left_train, right_train, disp_train_L, left_val, right_val, disp_val_L


def KITTI2012_listloader(filepath):
	left_fold = 'colored_0/'
	right_fold = 'colored_1/'
	disp_noc = 'disp_occ/'

	image = [img for img in os.listdir(filepath + left_fold) if img.find('_10') > -1]

	train = image[:]
	val = image[160:]

	left_train = [filepath + left_fold + img for img in train]
	right_train = [filepath + right_fold + img for img in train]
	disp_train = [filepath + disp_noc + img for img in train]

	left_val = [filepath + left_fold + img for img in val]
	right_val = [filepath + right_fold + img for img in val]
	disp_val = [filepath + disp_noc + img for img in val]

	return left_train, right_train, disp_train, left_val, right_val, disp_val


def is_image_file(filename):
	return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


def default_loader(path):
	return Image.open(path).convert('RGB')


def pfm_disparity_loader(path):
	return rp.readPFM(path)


class SceneFlowLoader(data.Dataset):
	def __init__(self, left, right, left_disparity, training, loader=default_loader, dploader=pfm_disparity_loader):

		self.left = left
		self.right = right
		self.disp_L = left_disparity
		self.loader = loader
		self.dploader = dploader
		self.training = training

	def __getitem__(self, index):
		left = self.left[index]
		right = self.right[index]
		disp_L = self.disp_L[index]

		left_img = self.loader(left)
		right_img = self.loader(right)
		dataL, scaleL = self.dploader(disp_L)
		dataL = np.ascontiguousarray(dataL, dtype=np.float32)

		if self.training:
			w, h = left_img.size
			th, tw = 256, 512

			x1 = random.randint(0, w - tw)
			y1 = random.randint(0, h - th)

			left_img = left_img.crop((x1, y1, x1 + tw, y1 + th))
			right_img = right_img.crop((x1, y1, x1 + tw, y1 + th))

			dataL = dataL[y1:y1 + th, x1:x1 + tw]

			processed = preprocess.get_transform(augment=False)
			left_img = processed(left_img)
			right_img = processed(right_img)

			return left_img, right_img, dataL
		else:
			w, h = left_img.size
			left_img = left_img.crop((w - 960, h - 544, w, h))
			right_img = right_img.crop((w - 960, h - 544, w, h))
			processed = preprocess.get_transform(augment=False)
			left_img = processed(left_img)
			right_img = processed(right_img)

			return left_img, right_img, dataL

	def __len__(self):
		return len(self.left)


def disparity_loader(path):
	return Image.open(path)


class KITTILoader(data.Dataset):
	def __init__(self, left, right, left_disparity, training, loader=default_loader, dploader=disparity_loader):

		self.left = left
		self.right = right
		self.disp_L = left_disparity
		self.loader = loader
		self.dploader = dploader
		self.training = training

	def __getitem__(self, index):
		left = self.left[index]
		right = self.right[index]
		disp_L = self.disp_L[index]

		left_img = self.loader(left)
		right_img = self.loader(right)
		dataL = self.dploader(disp_L)

		if self.training:
			w, h = left_img.size
			th, tw = 256, 512

			x1 = random.randint(0, w - tw)
			y1 = random.randint(0, h - th)

			left_img = left_img.crop((x1, y1, x1 + tw, y1 + th))
			right_img = right_img.crop((x1, y1, x1 + tw, y1 + th))

			dataL = np.ascontiguousarray(dataL, dtype=np.float32) / 256
			dataL = dataL[y1:y1 + th, x1:x1 + tw]

			processed = preprocess.get_transform(augment=False)
			left_img = processed(left_img)
			right_img = processed(right_img)

			return left_img, right_img, dataL
		else:
			w, h = left_img.size

			left_img = left_img.crop((w - 1232, h - 368, w, h))
			right_img = right_img.crop((w - 1232, h - 368, w, h))
			w1, h1 = left_img.size

			dataL = dataL.crop((w - 1232, h - 368, w, h))
			dataL = np.ascontiguousarray(dataL, dtype=np.float32) / 256

			processed = preprocess.get_transform(augment=False)
			left_img = processed(left_img)
			right_img = processed(right_img)

			return left_img, right_img, dataL

	def __len__(self):
		return len(self.left)


class SceneflowLoader(data.Dataset):
	def __init__(self, left, right, left_disparity, training, loader=default_loader, dploader=disparity_loader):

		self.left = left
		self.right = right
		self.disp_L = left_disparity
		self.loader = loader
		self.dploader = dploader
		self.training = training

	def __getitem__(self, index):
		left = self.left[index]
		right = self.right[index]
		disp_L = self.disp_L[index]

		left_img = self.loader(left)
		right_img = self.loader(right)
		dataL, scaleL = self.dploader(disp_L)
		dataL = np.ascontiguousarray(dataL, dtype=np.float32)

		if self.training:
			w, h = left_img.size
			th, tw = 256, 512

			x1 = random.randint(0, w - tw)
			y1 = random.randint(0, h - th)

			left_img = left_img.crop((x1, y1, x1 + tw, y1 + th))
			right_img = right_img.crop((x1, y1, x1 + tw, y1 + th))

			dataL = dataL[y1:y1 + th, x1:x1 + tw]

			processed = preprocess.get_transform(augment=False)
			left_img = processed(left_img)
			right_img = processed(right_img)

			return left_img, right_img, dataL
		else:
			w, h = left_img.size
			left_img = left_img.crop((w - 960, h - 544, w, h))
			right_img = right_img.crop((w - 960, h - 544, w, h))
			processed = preprocess.get_transform(augment=False)
			left_img = processed(left_img)
			right_img = processed(right_img)

			return left_img, right_img, dataL

	def __len__(self):
		return len(self.left)


def scene_listloader(filepath):
	classes = [d for d in os.listdir(filepath) if os.path.isdir(os.path.join(filepath, d))]
	image = [img for img in classes if img.find('frames_cleanpass') > -1]
	disp = [dsp for dsp in classes if dsp.find('disparity') > -1]

	monkaa_path = filepath + [x for x in image if 'monkaa' in x][0]
	monkaa_disp = filepath + [x for x in disp if 'monkaa' in x][0]

	monkaa_dir = os.listdir(monkaa_path)

	all_left_img = []
	all_right_img = []
	all_left_disp = []
	test_left_img = []
	test_right_img = []
	test_left_disp = []

	for dd in monkaa_dir:
		for im in os.listdir(monkaa_path + '/' + dd + '/left/'):
			if is_image_file(monkaa_path + '/' + dd + '/left/' + im):
				all_left_img.append(monkaa_path + '/' + dd + '/left/' + im)
				all_left_disp.append(monkaa_disp + '/' + dd + '/left/' + im.split(".")[0] + '.pfm')

		for im in os.listdir(monkaa_path + '/' + dd + '/right/'):
			if is_image_file(monkaa_path + '/' + dd + '/right/' + im):
				all_right_img.append(monkaa_path + '/' + dd + '/right/' + im)

	flying_path = filepath + [x for x in image if x == 'frames_cleanpass'][0]
	flying_disp = filepath + [x for x in disp if x == 'frames_disparity'][0]
	flying_dir = flying_path + '/TRAIN/'
	subdir = ['A', 'B', 'C']

	for ss in subdir:
		flying = os.listdir(flying_dir + ss)

		for ff in flying:
			imm_l = os.listdir(flying_dir + ss + '/' + ff + '/left/')
			for im in imm_l:
				if is_image_file(flying_dir + ss + '/' + ff + '/left/' + im):
					all_left_img.append(flying_dir + ss + '/' + ff + '/left/' + im)

				all_left_disp.append(
					flying_disp + '/TRAIN/' + ss + '/' + ff + '/left/' + im.split(".")[0] + '.pfm')

				if is_image_file(flying_dir + ss + '/' + ff + '/right/' + im):
					all_right_img.append(flying_dir + ss + '/' + ff + '/right/' + im)

	flying_dir = flying_path + '/TEST/'

	subdir = ['A', 'B', 'C']

	for ss in subdir:
		flying = os.listdir(flying_dir + ss)

		for ff in flying:
			imm_l = os.listdir(flying_dir + ss + '/' + ff + '/left/')
			for im in imm_l:
				if is_image_file(flying_dir + ss + '/' + ff + '/left/' + im):
					test_left_img.append(flying_dir + ss + '/' + ff + '/left/' + im)

				test_left_disp.append(
					flying_disp + '/TEST/' + ss + '/' + ff + '/left/' + im.split(".")[0] + '.pfm')

				if is_image_file(flying_dir + ss + '/' + ff + '/right/' + im):
					test_right_img.append(flying_dir + ss + '/' + ff + '/right/' + im)

	driving_dir = filepath + [x for x in image if 'driving' in x][0] + '/'
	driving_disp = filepath + [x for x in disp if 'driving' in x][0]

	subdir1 = ['35mm_focallength', '15mm_focallength']
	subdir2 = ['scene_backwards', 'scene_forwards']
	subdir3 = ['fast', 'slow']

	for i in subdir1:
		for j in subdir2:
			for k in subdir3:
				imm_l = os.listdir(driving_dir + i + '/' + j + '/' + k + '/left/')
				for im in imm_l:
					if is_image_file(driving_dir + i + '/' + j + '/' + k + '/left/' + im):
						all_left_img.append(driving_dir + i + '/' + j + '/' + k + '/left/' + im)
					all_left_disp.append(
						driving_disp + '/' + i + '/' + j + '/' + k + '/left/' + im.split(".")[0] + '.pfm')

					if is_image_file(driving_dir + i + '/' + j + '/' + k + '/right/' + im):
						all_right_img.append(driving_dir + i + '/' + j + '/' + k + '/right/' + im)

	return all_left_img, all_right_img, all_left_disp, test_left_img, test_right_img, test_left_disp
