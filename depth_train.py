import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
from torch.distributions.normal import Normal
# from torch.autograd import Variable
import torch.nn.functional as F
import skimage
import skimage.io
import skimage.transform
import numpy as np
import time
import math

# from input.depth_input import KITTIloader2015 as ls
# from input.depth_input import KITTILoader as DA
0
from depth import *
from input.depth_input import SceneFlowLoader
from input.depth_input import KITTILoader

# from  import convbn_3d

ttype = torch.float

parser = argparse.ArgumentParser(description='PSMNet')
parser.add_argument('--maxdisp', type=int, default=192,
                    help='maxium disparity')
parser.add_argument('--model', default='stackhourglass',
                    help='select model')
parser.add_argument('--datatype', default='2015',
                    help='datapath')
parser.add_argument('--datapath', default='/media/jiaren/ImageNet/data_scene_flow_2015/training/',
                    help='datapath')
parser.add_argument('--epochs', type=int, default=300,
                    help='number of epochs to train')
parser.add_argument('--loadmodel', default='./trained/submission_model.tar',
                    help='load model')
parser.add_argument('--savemodel', default='./',
                    help='save model')
parser.add_argument('--varmodel', default='./',
                    help='save model')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
torch.manual_seed(args.seed)
if args.cuda:
	torch.cuda.manual_seed(args.seed)

if args.datatype == '2015':
	from input.depth_input import KITTI2015_listloader as ls
elif args.datatype == '2012':
	from input.depth_input import KITTI2012_listloader as ls
elif args.datatype == 'sceneflow':
	from input.depth_input import scene_listloader as ls

all_left_img, all_right_img, all_left_disp, test_left_img, test_right_img, test_left_disp = ls(args.datapath)

if args.datatype == '2015' or args.datatype == '2012':
	TrainImgLoader = torch.utils.data.DataLoader(
		KITTILoader(all_left_img, all_right_img, all_left_disp, True),
		batch_size=5, shuffle=True, num_workers=5, drop_last=False)

	TestImgLoader = torch.utils.data.DataLoader(
		KITTILoader(test_left_img, test_right_img, test_left_disp, False),
		batch_size=1, shuffle=False, num_workers=1, drop_last=False)
elif args.datatype == 'sceneflow':
	TrainImgLoader = torch.utils.data.DataLoader(
		SceneFlowLoader(all_left_img, all_right_img, all_left_disp, True),
		batch_size=5, shuffle=True, num_workers=5, drop_last=False)

	TestImgLoader = torch.utils.data.DataLoader(
		SceneFlowLoader(test_left_img, test_right_img, test_left_disp, False),
		batch_size=1, shuffle=False, num_workers=1, drop_last=False)

model = PSMNet(args.maxdisp)

if args.cuda:
	model = nn.DataParallel(model)
# model.cuda()

if args.loadmodel is not None:
	state_dict = torch.load(args.loadmodel)
	model.load_state_dict(state_dict['state_dict'])

for param in model.parameters():
	param.requires_grad = False

# for param in model.module.feature_extraction.parameters():
# 	param.requires_grad = False
#

# maxdisp = 192
# model.module.disparityregression.pdf_reg = nn.Sequential(nn.Conv2d(maxdisp, int(maxdisp / 4), 1),
#                                                          # nn.BatchNorm2d(int(maxdisp / 4)),
#                                                          nn.ELU(),
#                                                          nn.Conv2d(int(maxdisp / 4), 2, 1))
# for m in model.module.disparityregression.modules():
# 	# if isinstance(m, nn.Conv2d):
# 	# 	n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
# 	# 	m.weight.data.normal_(0, math.sqrt(2. / n))
# 	# elif isinstance(m, nn.Conv3d):
# 	# 	n = m.kernel_size[0] * m.kernel_size[1] * m.kernel_size[2] * m.out_channels
# 	# 	m.weight.data.normal_(0, math.sqrt(2. / n))
# 	# elif isinstance(m, nn.BatchNorm2d):
# 	# 	m.weight.data.fill_(1)
# 	# 	m.bias.data.zero_()
# 	# elif isinstance(m, nn.BatchNorm3d):
# 	# 	m.weight.data.fill_(1)
# 	# 	m.bias.data.zero_()
# 	# elif isinstance(m, nn.Linear):
# 	# 	m.bias.data.zero_()
# 	for p in m.parameters():
# 		p.requires_grad = True

# for newm in (model.module.classif1, model.module.classif2, model.module.classif3):
# 	for m in newm.modules():
# 		for p in m.parameters():
# 			p.requires_grad = True

# for m in newm.__getitem__(2).parameters():
# 	param.requires_grad = True
# for param in model.module.parameters():
# 	param.requires_grad = False

# for param in model.module.classif1.__getitem__(2).parameters():
# 	param.requires_grad = True

# if args.loadmodel is not None:
# 	state_dict = torch.load(args.loadmodel)
# 	model.load_state_dict(state_dict['state_dict'])
# model.module.disparityregression = disparityregression(192)


model.module.type(ttype)
# model.module.disparityregression = x

print('Number of model parameters: {}'.format(sum([p.data.nelement() for p in model.parameters()])))
# torch.save({'state_dict': model.state_dict(), }, args.savemodel+"mod.tar")

if args.cuda:
	# model = nn.DataParallel(model)
	model.cuda()

optimizer = optim.Adam(model.parameters(), lr=0.1, betas=(0.9, 0.999))


def loss_with_sigma(x, mu, logsigma):
	e = torch.exp(-logsigma)
	x, mu = x * e, mu * e
	loss = F.smooth_l1_loss(x, mu, reduction='none') + 2 * logsigma + 2 * math.pi
	return torch.mean(loss)


maxdisp = args.maxdisp
DR = disparityregression(maxdisp)  # .type(torch.double)
var_model = nn.Sequential(nn.Conv2d(maxdisp, int(maxdisp / 4), 1),
                          # nn.BatchNorm2d(int(maxdisp / 4)),
                          nn.ELU(),
                          nn.Conv2d(int(maxdisp / 4), 1, 1)).cuda()

if args.varmodel is not None:
	state_dict = torch.load(args.varmodel)
	var_model.load_state_dict(state_dict['state_dict'])

for m in var_model.modules():
	# if isinstance(m, nn.Conv2d):
	# 	n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
	# 	m.weight.data.normal_(0, math.sqrt(2. / n))
	# elif isinstance(m, nn.Conv3d):
	# 	n = m.kernel_size[0] * m.kernel_size[1] * m.kernel_size[2] * m.out_channels
	# 	m.weight.data.normal_(0, math.sqrt(2. / n))
	# elif isinstance(m, nn.BatchNorm2d):
	# 	m.weight.data.fill_(1)
	# 	m.bias.data.zero_()
	# elif isinstance(m, nn.BatchNorm3d):
	# 	m.weight.data.fill_(1)
	# 	m.bias.data.zero_()
	# elif isinstance(m, nn.Linear):
	# 	m.bias.data.zero_()
	for p in m.parameters():
		p.requires_grad = True


def train(imgL, imgR, disp_L):
	model.train()
	# imgL = Variable(torch.FloatTensor(imgL))
	# imgR = Variable(torch.FloatTensor(imgR))
	# disp_L = Variable(torch.FloatTensor(disp_L))

	# imgL = torch.from_numpy(imgL)
	# imgR = torch.from_numpy(imgR)
	# disp_L = torch.from_numpy(disp_L)
	imgL, imgR, disp_true = imgL.type(ttype), imgR.type(ttype), disp_L.type(ttype)

	if args.cuda:
		imgL, imgR, disp_true = imgL.cuda(), imgR.cuda(), disp_L.cuda()

	mask = (disp_true > 0)
	mask.detach_()

	optimizer.zero_grad()

	output1_, output2_, output3_ = model(imgL, imgR)
	# print(output3_)
	# output1 = torch.squeeze(output1[:,0],1)
	# output2 = torch.squeeze(output2[:,0,:,:],1)
	# output3 = torch.squeeze(output3[:,0,:,:],1)
	# var1 = torch.squeeze(output1[:,1,:,:],1)
	# var2 = torch.squeeze(output2[:,1,:,:],1)
	# var3 = torch.squeeze(output3[:,1,:,:],1)
	# output1_, output2_, output3_ = output1_.type(torch.double), output2_.type(torch.double), \
	#                                output3_.type(torch.double)
	# disp_true = disp_true.type(torch.double)
	# print(output1_.size())
	output1, output2, output3 = DR(output1_), DR(output2_), DR(output3_)
	var1, var2, var3 = var_model(output1_), var_model(output2_), var_model(output3_)
	# print(var3.size())
	var1, var2, var3 = torch.squeeze(var1), torch.squeeze(var2), torch.squeeze(var3),
	# evar1, evar2, evar3 = torch.exp(var1), torch.exp(var2), torch.exp(var3)
	# print(output1)
	# m1 = Normal(output1, var1)
	# m2 = Normal(output2, var2)
	# m3 = Normal(output3, var3)
	# print(output1.size())
	# print(var1.size())

	# loss = 0.5*F.smooth_l1_loss(output1[mask], disp_true[mask], reduction='none')/var1[mask]/2 + 0.7*F.smooth_l1_loss(output2[mask], disp_true[mask], reduction='none')/var2[mask]/2 + F.smooth_l1_loss(output3[mask], disp_true[mask], reduction='none')/var3[mask]/2
	# loss = torch.mean(loss)
	# loss += 0.5*(0.25 * torch.mean(torch.log(var1[mask])) + 0.7*0.5 * torch.mean(torch.log(var2[mask])) + 0.5 * torch.mean(torch.log(var3[mask])))
	# loss = 0.5 * F.smooth_l1_loss(output1[mask], disp_true[mask], reduction='none') \
	#        * evar1[mask] + 0.7 * F.smooth_l1_loss(output2[mask], disp_true[mask], reduction='none') \
	#        * evar2[mask] + F.smooth_l1_loss(output3[mask], disp_true[mask], reduction='none') * evar3[mask]
	# # loss = torch.mean(loss)
	# loss += 0.2 * (0.5 * var1[mask] + 0.7 * var2[mask] + 1 * var3[mask])
	# loss = torch.mean(loss)

	# output1, output2, output3 = m1.rsample(), m2.rsample(), m3.rsample()
	# print(output3)
	# loss = 0.5 * F.smooth_l1_loss(output1[mask], disp_true[mask]) + \
	#        0.7 * F.smooth_l1_loss(output2[mask], disp_true[mask]) + \
	#        1.0 * F.smooth_l1_loss(output3[mask], disp_true[mask])
	loss = 0.5 * loss_with_sigma(output1[mask], disp_true[mask], var1[mask]) \
	       + 0.7 * loss_with_sigma(output2[mask], disp_true[mask], var2[mask]) \
	       + 1.0 * loss_with_sigma(output3[mask], disp_true[mask], var3[mask])
	loss.backward()
	optimizer.step()

	# return loss.data[0]
	return loss.item()


def test(imgL, imgR, disp_true):
	model.eval()
	imgL = torch.FloatTensor(imgL)
	imgR = torch.FloatTensor(imgR)
	if args.cuda:
		imgL, imgR = imgL.cuda(), imgR.cuda()

	with torch.no_grad():
		output3 = model(imgL, imgR)

	pred_disp = output3.data.cpu()
	pred_disp = torch.squeeze(pred_disp[:, 0], 1)
	pred_disp = torch.squeeze(pred_disp, 0)
	# computing 3-px error#
	print(disp_true.size())
	true_disp = disp_true
	# index = np.argwhere(true_disp>0)
	nz = torch.nonzero(true_disp > 0)
	print(nz.size())
	index = torch.transpose(nz, 0, 1)
	# print(index.shape)
	disp_true[index[0][:], index[1][:], index[2][:]] = np.abs(
		true_disp[index[0][:], index[1][:], index[2][:]] - pred_disp[index[0][:], index[1][:], index[2][:]])
	correct = (disp_true[index[0][:], index[1][:], index[2][:]] < 3) | (
		  disp_true[index[0][:], index[1][:], index[2][:]] < true_disp[
		index[0][:], index[1][:], index[2][:]] * 0.05)
	# disp_true[index[:][0], index[:][1], index[:][2]] = np.abs(true_disp[index[:][0], index[:][1], index[:][2]]-pred_disp[index[:][0], index[:][1], index[:][2]])
	# correct = (disp_true[index[:][0], index[:][1], index[:][2]] < 3)|(disp_true[index[:][0], index[:][1], index[:][2]] < true_disp[index[:][0], index[:][1], index[:][2]]*0.05)
	torch.cuda.empty_cache()

	return 1 - (float(torch.sum(correct)) / float(len(index[0])))


def adjust_learning_rate(optimizer, epoch):
	if epoch <= 5:
		lr = 1e-8
	elif epoch <= 10:
		lr = 1e-9
	elif epoch <= 20:
		lr = 1e-10
	else:
		lr = 1e-11
	print(lr)
	for param_group in optimizer.param_groups:
		param_group['lr'] = lr


def main():
	max_acc = 0
	max_epo = 0
	start_full_time = time.time()
	optimizer.zero_grad()

	for epoch in range(1, args.epochs + 1):
		total_train_loss = 0
		total_test_loss = 0
		adjust_learning_rate(optimizer, epoch)

		# for batch_idx, (imgL, imgR, disp_L) in enumerate(TestImgLoader):
		# 	print(np.nonzero(disp_L > 0).shape)
		# 	test_loss = test(imgL, imgR, disp_L)
		# 	print('Iter %d 3-px error in val = %.3f' % (batch_idx, test_loss * 100))
		# 	total_test_loss += test_loss

		## training ##
		for batch_idx, (imgL_crop, imgR_crop, disp_crop_L) in enumerate(TrainImgLoader):
			start_time = time.time()

			loss = train(imgL_crop, imgR_crop, disp_crop_L)
			print('Iter %d training loss = %.4f , time = %.3f' % (batch_idx, loss, time.time() - start_time))
			total_train_loss += loss
		print('epoch %d total training loss = %.4f' % (epoch, total_train_loss / len(TrainImgLoader)))

		savefilename = args.savemodel + 'finetune_' + str(epoch) + '.tar'
		torch.save({
			'epoch': epoch,
			'state_dict': var_model.state_dict(),
			'train_loss': total_train_loss / len(TrainImgLoader),
			'test_loss': total_test_loss / len(TestImgLoader) * 100,
		}, savefilename)

		## Test ##

		# for batch_idx, (imgL, imgR, disp_L) in enumerate(TestImgLoader):
		# 	test_loss = test(imgL, imgR, disp_L)
		# 	print('Iter %d 3-px error in val = %.3f' % (batch_idx, test_loss * 100))
		# 	total_test_loss += test_loss
		#
		# print('epoch %d total 3-px error in val = %.3f' % (epoch, total_test_loss / len(TestImgLoader) * 100))
		# if total_test_loss / len(TestImgLoader) * 100 > max_acc:
		# 	max_acc = total_test_loss / len(TestImgLoader) * 100
		# 	max_epo = epoch
		# print('MAX epoch %d total test error = %.3f' % (max_epo, max_acc))

		# SAVE

		print('full finetune time = %.2f HR' % ((time.time() - start_full_time) / 3600))
	print(max_epo)
	print(max_acc)


if __name__ == '__main__':
	main()
